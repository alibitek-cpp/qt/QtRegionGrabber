/*
 *  Copyright (C) 2007 Montel Laurent <montel@kde.org>
 *  Copyright (C) 2010 Pau Garcia i Quiles <pgquiles@elpauer.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "kbackgroundsnapshot.h"
#include "regiongrabber.h"

#include <QMouseEvent>
#include <QPixmap>
#include <QDesktopWidget>
#include <QApplication>

KBackgroundSnapshot::KBackgroundSnapshot()
    : KSnapshotObject()
{   
    grabber = new QWidget( 0,  Qt::X11BypassWindowManagerHint );
    grabber->move( -1000, -1000 );
    grabber->installEventFilter( this );
    grabber->show();
    grabber->grabMouse( Qt::WaitCursor );

    grabRegion();

    grabber->releaseMouse();
    grabber->hide();
}

KBackgroundSnapshot::~KBackgroundSnapshot()
{
    //kDebug()<<" KBackgroundSnapshot::~KBackgroundSnapshot()\n";
}

void KBackgroundSnapshot::performGrab()
{
    //kDebug()<<"KBackgroundSnapshot::performGrab()\n";
    grabber->releaseMouse();
    grabber->hide();

    snapshot = QPixmap::grabWindow( QApplication::desktop()->winId() );
}

void KBackgroundSnapshot::grabRegion()
{
   QRect emptySelection;
   rgnGrab = new RegionGrabber(emptySelection);
   connect( rgnGrab, SIGNAL(regionGrabbed(QPixmap)),
                     SLOT(slotRegionGrabbed(QPixmap)) );

}


void KBackgroundSnapshot::slotRegionGrabbed( const QPixmap &pix )
{
  if ( !pix.isNull() )
    snapshot = pix;

  rgnGrab->deleteLater();
  QApplication::restoreOverrideCursor();
}

bool KBackgroundSnapshot::eventFilter( QObject* o, QEvent* e)
{
    if ( o == grabber && e->type() == QEvent::MouseButtonPress )
    {
        QMouseEvent* me = (QMouseEvent*) e;

        if ( QWidget::mouseGrabber() != grabber )
            return false;

        if ( me->button() == Qt::LeftButton )
            performGrab();
    }

    return false;
}
