#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QDebug>
#include <QFile>
#include <QRubberBand>
#include <QSizeGrip>
#include <QStyle>
#include <QDesktopWidget>
#include <QKeyEvent>
#include <QScreen>
#include "kbackgroundsnapshot.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  rgnGrab( 0 ),
  modified(true),
  savedPosition(QPoint(-1, -1))
{
  ui->setupUi(this);

  setGeometry(
      QStyle::alignedRect(
          Qt::LeftToRight,
          Qt::AlignCenter,
          size(),
          qApp->desktop()->availableGeometry()
      )
  );

  QPixmap image("/home/alpha/Downloads/QtRegionGrabber/audi_PNG1771.png");

  ui->label->setPixmap(image);

//  Resizable_rubber_band* band = new Resizable_rubber_band(ui->label);
//  band->move(100, 100);
//  band->resize(100, 100);
//  band->setMinimumSize(30, 30);

  //band->show();
  //QSizeGrip* g = new QSizeGrip(band);

  connect( &updateTimer, SIGNAL(timeout()), this, SLOT(updatePreview()) );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_R)
    {
        qDebug() << "hi";

        grabRegion();

//        Resizable_rubber_band* band = new Resizable_rubber_band(ui->label);
//        band->move(rect_x - 50, 100);
//        band->resize(100, 100);
//        band->setMinimumSize(30, 30);

    }
}

Resizable_rubber_band::Resizable_rubber_band(QWidget *parent) : QWidget(parent) {
  //tell QSizeGrip to resize this widget instead of top-level window
  setWindowFlags(Qt::SubWindow);

  QHBoxLayout* layout = new QHBoxLayout(this);
  layout->setContentsMargins(0, 0, 0, 0);

  QSizeGrip* grip1 = new QSizeGrip(this);
  QSizeGrip* grip2 = new QSizeGrip(this);

  layout->addWidget(grip1, 0, Qt::AlignLeft | Qt::AlignTop);
  layout->addWidget(grip2, 0, Qt::AlignRight | Qt::AlignBottom);

  rubberband = new QRubberBand(QRubberBand::Rectangle, this);
  rubberband->move(0, 0);
  rubberband->show();
}

void Resizable_rubber_band::resizeEvent(QResizeEvent *) {
  rubberband->resize(size());
}

void Resizable_rubber_band::mousePressEvent(QMouseEvent *event)
{
    origin = event->pos();
    if (!rubberband)
        rubberband = new QRubberBand(QRubberBand::Rectangle, this);
    rubberband->setGeometry(QRect(origin, QSize()));
    //rubberband->show();
}

void Resizable_rubber_band::mouseMoveEvent(QMouseEvent *event)
{
    rubberband->setGeometry(QRect(origin, event->pos()).normalized());
}

void Resizable_rubber_band::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event)
    rubberband->hide();
    // determine selection, for example using QRect::intersects()
    // and QRect::contains().
}

void MainWindow::grabRegion()
{
   rgnGrab = new RegionGrabber(lastRegion);
   connect( rgnGrab, SIGNAL(regionGrabbed(QPixmap)),
                     SLOT(slotRegionGrabbed(QPixmap)) );
   connect( rgnGrab, SIGNAL(regionUpdated(QRect)),
                     SLOT(slotRegionUpdated(QRect)) );

}

void MainWindow::slotRegionGrabbed( const QPixmap &pix )
{
   qDebug() << "slotRegionGrabbed()";

  if ( !pix.isNull() )
  {
    snapshot = pix;
    updatePreview();
    modified = true;
  }

  rgnGrab->deleteLater();

  QApplication::restoreOverrideCursor();
  show();
}

void MainWindow::slotRegionUpdated( const QRect &selection )
{
    qDebug() << "slotRegionUpdated()";
    lastRegion = selection;
}

void MainWindow::updatePreview()
{
    qDebug() << "updatePreview()";
    setPreview( snapshot );
}

void MainWindow::setPreview( const QPixmap &pm )
{
    qDebug() << "Preview of the snapshot image (" << pm.width() << " x " << pm.height() << ")";
}
