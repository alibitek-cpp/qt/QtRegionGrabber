#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDialog>
#include <QRubberBand>
#include <QLabel>
#include <QTimer>
#include "regiongrabber.h"

namespace Ui {
class MainWindow;
}

class Resizable_rubber_band : public QWidget {
public:
  Resizable_rubber_band(QWidget* parent = 0);

private:
  QRubberBand* rubberband;
  QPoint origin;

protected:
  void resizeEvent(QResizeEvent *event);
  void mousePressEvent(QMouseEvent *event);
  void mouseMoveEvent(QMouseEvent *event);
  void mouseReleaseEvent(QMouseEvent *event);
};


class MainWindow : public QMainWindow
{
  Q_OBJECT
  
public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();
  
protected:
    void keyPressEvent(QKeyEvent* event);

private:
    void performGrab();
    void grabRegion();

private slots:
    void setPreview( const QPixmap &pm );
    void updatePreview();
    void slotRegionGrabbed( const QPixmap & );
    void slotRegionUpdated( const QRect & );

private:
  Ui::MainWindow *ui;
  int rect_x = 250;

  RegionGrabber *rgnGrab;

  QPixmap snapshot;
  QRect lastRegion;

  bool modified;
  QPoint savedPosition;
  QTimer updateTimer;
};

#endif // MAINWINDOW_H
