QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RegionGrabber
TEMPLATE = app


SOURCES += main.cpp \
        MainWindow.cpp \
        regiongrabber.cpp \
        ksnapshotobject.cpp \
        kbackgroundsnapshot.cpp

HEADERS  += MainWindow.h regiongrabber.h ksnapshotobject.h kbackgroundsnapshot.h

FORMS    += MainWindow.ui

QMAKE_CXXFLAGS += -std=c++14
